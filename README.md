Pour lancer l'applciation :
- Télécharger le projet du Git
- Se positionner dans /BeerAppGleamer
- Dans le dossier /BeerFront ouvrir un terminal et lancer "npm install" (pour installer les dépendances)
- Dans le même dossier lancer "npm start"
- Dans le dossier /BeerBack ouvrir un terminal et lancer "mvn clean install" (pour installer les dépendances)
- Dans le même dossier lancer "mvn spring-boot:run" (le port utilisé est le 8080, veuillez vérifier que ce port est libre)
- Ouvrir un navigateur web et lancer "http://localhost:3000"

Il vous suffira alors de taper le nom ou une partie d'un nom de bière
une liste s'affichera alors.
L'image est cliquable et affichera le pourcentage d'alcool.
"Description" est également cliquable et fait office de 
"read more".

Concernant la question de comment éviter la limite de l'API qui n'accepte que 1 req/s,
Il serait, je pense, judicieux de penser à un synchronisateur dans le côté Back du style sémaphore
qui travaillera avec un jeton qui sera pris en cas de requête et libéré lors de la fin de cette dernière.