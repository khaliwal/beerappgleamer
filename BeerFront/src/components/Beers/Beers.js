import React from 'react'
import Beer from '../Beer/Beer'
import '../Beer/Beer.css';

const Beers=(props)=>{
    return(
        props.mesbieres.map((b,index)=>{
            if (b.length === 0) {
                return(console.log("aaa"));
            }
            return (<table><Beer
                key = {b.id}
                nomDeBiere = {b.name}
                image_url = {b.image_url}
                desc = {b.description}
                alco = {b.method.fermentation.temp.value}
                 /></table>)
        })
    )
}
export default Beers