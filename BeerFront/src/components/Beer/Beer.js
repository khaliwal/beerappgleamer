import React, {useState} from 'react'
import './Beer.css';


//stateless component

const Beer =(props)=>{
    const [readMore,setReadMore]=useState(false);
    const imageClick = () => {
        alert("Pourcentage : " + props.alco + "°")
    } 
    return(<tr border="4" cellspacing="4" cellpadding="4" width="80%" >
            <td><p id="name">{props.nomDeBiere} </p></td>
            <td id="img"><img src={props.image_url} height="70" width="" onClick={() => imageClick()}/></td>
            <td>
            <a onClick={()=>{setReadMore(!readMore)}}><h2>Description :</h2></a>
      {readMore && props.desc}</td></tr>
            
        
    )
}

export default Beer
