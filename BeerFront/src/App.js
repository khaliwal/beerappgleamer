import logo from './logo.svg';
import './App.css';
import React, {Component} from 'react';
import ListBeers from './components/Beers/Beers'
import Button from 'react-bootstrap/Button';
import Input from 'react-bootstrap/Button';


class App extends Component {
  constructor(props){
    super(props);
  }
  state={
    inputHandler:'',
    MyBeers:[]
  }
  inputHandlerFunction=(event)=>{
    this.setState({inputHandler:event.target.value})
  }
  search=()=>{
    console.log("ce que walid cherche",this.state.inputHandler)
    this.ApiCall(this.state.inputHandler)
  }
  ApiCall=(biere)=>{
    fetch("http://localhost:8080/beers/" + biere,{
      method:'GET',
      header:{
        'Accept':'application/json',
        'Content-Type':'application/json',
        'User-Agent': ''
      },
  }).then((response)=>response.json())
  .then((r)=>{
    this.setState({MyBeers:r})
    console.log("ma biere",r)
  })
  .catch((error)=>{
    console.log("erreur de connextion",error.toString());
  })

  }
  render(){
    // let Beers= null;
    let Beers=(
    <ListBeers mesbieres={this.state.MyBeers} />
    )
    return (
      <div className="App">
      <h1> Vous recherchez une bière ?</h1>
      <form>
      <input onChange={(event)=>this.inputHandlerFunction(event)}/>
      <br/>
      <Button onClick={this.search} disabled={!this.state.inputHandler}> rechercher</Button>
      {Beers}
      </form>
      </div>
    );
  }
  
}

export default App;
