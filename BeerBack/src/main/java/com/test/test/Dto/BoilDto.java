package com.test.test.Dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BoilDto implements Serializable {
    private int value;
    private String unit;

    public BoilDto(int value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public BoilDto() {

    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
