package com.test.test.Dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Bean;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeerDto implements Serializable {
    private int id;
    private String name;
    private String tagline;
    private String description;
    private String first_brewed;
    private String image_url;
    private float attenuation_level;
    private BoilDto boil_volume;
    private MethodDto method;

    public BeerDto(int id, String name, String tagline, String description, String first_brewed, String image_url,
                   float attenuation_level, BoilDto boil_volume, MethodDto method) {
        this.id = id;
        this.name = name;
        this.tagline = tagline;
        this.description = description;
        this.first_brewed = first_brewed;
        this.image_url= image_url;
        this.attenuation_level = attenuation_level;
        this.boil_volume = boil_volume;
        this.method = method;
    }

    public BeerDto() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagline() {
        return tagline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BoilDto getBoil_volume() {
        return boil_volume;
    }

    public void setBoil_volume(BoilDto boil_volume) {
        this.boil_volume = boil_volume;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getFirst_brewed() {
        return first_brewed;
    }

    public void setFirst_brewed(String first_brewed) {
        this.first_brewed = first_brewed;
    }

    public float getAttenuation_level() {
        return attenuation_level;
    }

    public void setAttenuation_level(float attenuation) {
        this.attenuation_level = attenuation;
    }

    public MethodDto getMethod() {
        return method;
    }

    public void setMethod(MethodDto method) {
        this.method = method;
    }
}
