package com.test.test.Dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown =  true)
public class Mash_TempDto implements Serializable {
    private TempDto temp;
    private int duration;

    public Mash_TempDto(TempDto temp, int duration) {
        this.temp = temp;
        this.duration = duration;
    }

    public Mash_TempDto () {

    }

    public TempDto getTemp() {
        return temp;
    }

    public void setTemp(TempDto temp) {
        this.temp = temp;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
