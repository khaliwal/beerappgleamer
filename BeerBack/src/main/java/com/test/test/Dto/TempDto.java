package com.test.test.Dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TempDto implements Serializable {
    private int value;
    private String unit;

    public TempDto(int value, String unit) {
        this.value = value;
        this.unit = unit;
    }

    public TempDto () {

    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
