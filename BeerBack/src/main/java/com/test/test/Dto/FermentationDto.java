package com.test.test.Dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FermentationDto implements Serializable {
    private TempDto temp;

    public FermentationDto(TempDto temp) {
        this.temp = temp;
    }

    public FermentationDto() {

    }

    public TempDto getTemp() {
        return temp;
    }

    public void setTemp(TempDto temp) {
        this.temp = temp;
    }
}
