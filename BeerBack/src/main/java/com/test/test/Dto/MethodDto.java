package com.test.test.Dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MethodDto implements Serializable {
    private List<Mash_TempDto> mash_temp;
    private FermentationDto fermentation;

    public MethodDto(List<Mash_TempDto> mash_temp, FermentationDto fermentation) {
        this.mash_temp = mash_temp;
        this.fermentation = fermentation;
    }

    public MethodDto () {

    }

    public List<Mash_TempDto> getMash_temp() {
        return mash_temp;
    }

    public void setMash_temp(List<Mash_TempDto> mash_temp) {
        this.mash_temp = mash_temp;
    }

    public FermentationDto getFermentation() {
        return fermentation;
    }

    public void setFermentation(FermentationDto fermentation) {
        this.fermentation = fermentation;
    }
}
