package com.test.test.controllers;

import com.test.test.Dto.BeerDto;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin("*")
public class ControllerBeer {
    static final String link_api = "https://api.punkapi.com/v2/beers?beer_name=";
    @GetMapping(value="/beers/{name}")
    public ResponseEntity beerList(@PathVariable String name) {
        RestTemplate restTemplate = new RestTemplate();
        String f = link_api + name;
        HttpHeaders headers = new HttpHeaders();
        headers.add("user-agent", "");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
        if (entity == null) {
            return new ResponseEntity("API Punk non accessible !", HttpStatus.NOT_FOUND);
        }


        BeerDto[] response = restTemplate.exchange(f, HttpMethod.GET, entity, BeerDto[].class).getBody();
        return new ResponseEntity(response, HttpStatus.OK);
    }


}
